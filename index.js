const Eris = require('eris');
const nano = require('nano')('http://localhost:5984');
const express = require('express');

const Config = require('./config.json');

const route = require('./router');

const app = express();

// SET UP HTTP
app.use(express.static('public'))
app.get('/ping', (req, res) => res.send('pong!'));
app.listen(Config.PORT, () => console.log('HTTP on port ' + Config.PORT));

// SEt UP BOT
var bot = new Eris(Config.TOKEN, {
    largeThreshold: 1 // how many offline users to grab
});

bot.on('ready', () => {
    console.log('Connected to Discord');
    bot.editStatus('online', {
        name: '!help',
        type: 0
    });
});

bot.on('messageCreate', (msg) => route(bot, msg));
bot.connect();
