const Help = require('./cmd/help');
const Order = require('./cmd/order');
const Say = require('./cmd/say');
const Menu = require('./cmd/menu');

module.exports = (bot, msg) => {
    if (msg.bot) return;

    if (Order.scan(bot, msg)) return;

    if (msg.content.startsWith("!help")) Help(bot, msg);
    else if (msg.content.startsWith("!order")) Order.order(bot, msg);
    else if (msg.content.startsWith("!say")) Say(bot, msg);
    else if (msg.content.startsWith("!menu")) Menu(bot, msg);
};
