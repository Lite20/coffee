module.exports = function (bot, msg) {
    bot.createMessage(msg.channel.id, {
        embed: {
            color: 0xe1cd97,
            fields: [
                {
                    name: "!menu",
                    value: "see our menu",
                    inline: false
                },
                {
                    name: "!order",
                    value: "summon a staff member to place an order",
                    inline: false
                }
            ],
            footer: {
                text: "amai café ☕"
            }
        }
    });
};
