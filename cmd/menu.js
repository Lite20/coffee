module.exports = function (bot, msg) {
    bot.createMessage(msg.channel.id, {
        embed: {
            color: 0xe1cd97,
            fields: [
                {
                    name: "Coffee",
                    value: "A plain and simple, traditional coffee",
                    inline: false
                }
            ],
            footer: {
                text: "amai café ☕"
            }
        }
    });
};
