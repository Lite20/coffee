const Config = require('../config.json');
const Messages = require('../messages.json');

let orders = {};

module.exports = {
    order: (bot, msg) => {
        if (typeof orders[msg.author.id] !== "undefined") {
            bot.createMessage(msg.channel.id, Messages.en.orderWait);
        } else {
            orders[msg.author.id] = {
                items: [],
                waitress: 0,
                channel: msg.channel.id
            };

            thumbsay(bot, msg, "Hello, <@" + msg.author.id + ">, what can I get for you? ", "'nvm' to cancel.");
        }
    },
    scan: (bot, msg) => {
        // make sure an order is in progress
        if (typeof orders[msg.author.id] === "undefined") return false;

        // make sure message came from original channel
        if (msg.channel.id !== orders[msg.author.id].channel) return false;

        // make sure not a command
        if (msg.content[0] === '!') return false;

        // cancel?
        if (cancelling(msg.content)) {
            orders[msg.author.id] = undefined;
            thumbsay(
                bot, msg,
                'Oh, okay. If you change your mind, feel free to `!order`!'
            );

            return true;
        }

        // check if terminating order
        if (terminating(msg.content)) {
            if (orders[msg.author.id].items.length === 0) {
                thumbsay(
                    bot, msg,
                    'Oh, okay. If you change your mind, feel free to `!order`!'
                );

                orders[msg.author.id] = undefined;
            } else {
                thumbsay(
                    bot, msg,
                    'Alrighty! Your order will be ready soon!'
                );

                let order = orders[msg.author.id];
                for (let i = 0; i < order.items.length; i++) {
                    let item = orders[msg.author.id].items[i];
                    setTimeout(() => {
                        bot.createMessage(msg.channel.id, {
                            embed: {
                                color: 0xe1cd97,
                                fields: [
                                    {
                                        name: Config.waitresses[order.waitress].name,
                                        author: {
                                            name: Config.waitresses[order.waitress].name,
                                            url:  Config.waitresses[order.waitress].url,
                                            icon_url:  Config.waitresses[order.waitress].image,
                                            proxy_icon_url:  Config.waitresses[order.waitress].image,
                                        },
                                        value: 'Here is your **' + item + '**, <@' + msg.author.id + '>!'
                                    }
                                ],
                                footer: {
                                    text: 'amai café ☕'
                                }
                            }
                        });
                    }, Config.menu[item].wait * 60000);
                }

                orders[msg.author.id] = undefined;
            }

            return true;
        }

        // add new item to order
        let item = msg.content.replace('!order', '');
        if (Config.menu.hasOwnProperty(item)) {
            orders[msg.author.id].items.push(item);
            say(
                bot, msg,
                'Okay, a **' + item + '** (' + Config.menu[item].wait + 'm)... anything else?',
                '\'no\' to finish. \'nvm\' to cancel.'
            );

            return true;
        } else {
            say(
                bot, msg,
                'I\'m afraid we don\'t carry **' + item + '**... anything else? (`!menu` to see menu)',
                '\'no\' to finish. \'nvm\' to cancel.'
            );

            return true;
        }
    }
}

function thumbsay (bot, msg, message, footer) {
    let order = orders[msg.author.id];
    bot.createMessage(msg.channel.id, {
        embed: {
            color: 0xe1cd97,
            author: {
                name: Config.waitresses[order.waitress].name,
                url: Config.waitresses[order.waitress].url
            },
            thumbnail: {
                url: Config.waitresses[order.waitress].image,
                proxy_url: Config.waitresses[order.waitress].image,
                width: Config.waitresses[order.waitress].width,
                height: Config.waitresses[order.waitress].height
            },
            description: message,
            footer: {
                text: (typeof footer === 'undefined' ? 'amai café ☕' : footer)
            }
        }
    });
}

function say (bot, msg, message, footer) {
    let order = orders[msg.author.id];
    bot.createMessage(msg.channel.id, {
        embed: {
            color: 0xe1cd97,
            author: {
                name: Config.waitresses[order.waitress].name,
                url:  Config.waitresses[order.waitress].url,
                icon_url:  Config.waitresses[order.waitress].image,
                proxy_icon_url:  Config.waitresses[order.waitress].image,
            },
            description: message,
            footer: {
                text: (typeof footer === 'undefined' ? 'amai café ☕' : footer)
            }
        }
    });
}

function terminating(msg) {
    return (msg.indexOf('no') >= 0);
}

function cancelling(msg) {
    if (msg.indexOf('cancel') >= 0) return true;
    else if (msg.indexOf('nvm') >= 0) return true;
    else if (msg.indexOf('nevermind') >= 0) return true;
}
